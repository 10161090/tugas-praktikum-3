package com.taufiqur_rachmad_10161090.praktikum3t;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Persistance extends AppCompatActivity {

    EditText tInput;
    Button tSave;
    TextView tOutput;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_persistance);

        tInput = findViewById(R.id.tInput);
        tSave = findViewById(R.id.tSave);
        tOutput = findViewById(R.id.tOutput);

        sharedPreferences = getSharedPreferences("datapersistance", MODE_PRIVATE);

        tSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData();
                Toast.makeText(Persistance.this, "Data Tersimpan!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getData() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("simpan_data", tInput.getText().toString() );
        editor.apply();
        tOutput.setText("Data Tersimpan :" + sharedPreferences.getString("simpan_data", null));


    }
}